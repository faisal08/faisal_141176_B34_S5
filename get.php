<!DOCTYPE HTML>
    <html>
           <head>
               <meta charset="utf-8">
               <meta name="viewport" content="width=device-width, initial-scale=1">
               <link rel="stylesheet" href="css/bootstrap.min.css">
               <title>Using Get Method</title>
           </head>
<body>
    <div class="container">
        <h2>GET FORM</h2>
        <form class="form-horizontal" action="captureGet.php" role="form"method="GET">
            <div class="form-group">
            <label class="control-label col-sm-2" for="text" >Name</label>
                <div class="col-sm-5">
                    <input type="text"class="form-control"id="text"placeholder="enter name"name="name">
                    </div>
                </div>
            <div class="form-group">
                <label class="control-label col-sm-2"for="email">EMAIL</label>
                <div class="col-sm-5">
                    <input type="text"class="form-control"id="email"placeholder="enter email"name="email">
                    </div>
                </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="button" class="btn btn-default"value="SUBMIT"name="button">Submit</button>
                    </div>
                </div>
            </form>
        </div>
</body>

</body>
</html>